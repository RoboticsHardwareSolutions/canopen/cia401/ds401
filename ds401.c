#include "canfestival.h"
#include "ObjDict.h"
#include "io_handlers.h"
#include "can_driver.h"
#include "timers_driver.h"
#include "rcan.h"
#include "ObjDict.h"
#include "time.h"

unsigned char inputs;
unsigned char nodeID;
unsigned char digital_input[1] = {0};
unsigned char digital_output[1] = {0};
static Message m = Message_Initializer;        // contain a CAN message

static void init_fake_callback(CO_Data *d, UNS32 id);

static void update_id_and_io(void);

static void outs_set(unsigned char);

static unsigned char inputs_get_current_state(void);

void ds401_init(void) {

    srand(time(NULL));

    if (canOpen(SOCKET_VCAN0, 1000000, &ObjDict_Data) != 0) {
        printf("can not started\r\n");
        canClose(&ObjDict_Data);
    } else {
        printf("can started !\r\n");
    }

    TimerInit();
    StartTimerLoop(init_fake_callback);
    nodeID = 0x1;
    //nodeID = read_bcd();
    setNodeId(&ObjDict_Data, nodeID);
    setState(&ObjDict_Data, Initialisation);
}

static void update_id_and_io(void) {
    digital_input[0] = inputs_get_current_state();
    digital_input_handler(&ObjDict_Data, digital_input, sizeof(digital_input));
    digital_output_handler(&ObjDict_Data, digital_output, sizeof(digital_output));
    outs_set(digital_output[0]);

//    if (!(nodeID == read_bcd())) {
//        nodeID = read_bcd();                               // Save the new CAN adress
//        setState(&ObjDict_Data, Stopped);         // Stop the node, to change the node ID
//        setNodeId(&ObjDict_Data, nodeID);                  // Now the CAN adress is changed
//        setState(&ObjDict_Data, Pre_operational); // Set to Pre_operational, master must boot it again
//    }
}

void ds401_loop(void) {
    usleep(150);
    update_id_and_io();
}

void ds401_stop(void) {
    StopTimerLoop(init_fake_callback);
    canClose(&ObjDict_Data);
}


static unsigned char inputs_get_current_state(void) {
    return (unsigned char) rand();      // Returns a pseudo-random integer between 0 and RAND_MAX.
}

static void outs_set(unsigned char outs) {
    //printf("OUTS SET =  %d\r\n", (int) outs);
}

static void init_fake_callback(CO_Data *d, UNS32 id) {
    (void) d;
    (void) id;
}
set(DS401_DIRECTORIES  ds401 )
set(DS401_EXECUTABLE_FILES
        ds401/ds401.c
        ds401/ds401.h
        ds401/ObjDict.c
        ds401/ObjDict.h
        ds401/io_handlers.c
        ds401/io_handlers.h)

#file(GLOB_RECURSE DS401_EXECUTABLE_FILES  "ds401/*.*" )
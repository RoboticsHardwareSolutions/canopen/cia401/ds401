#ifndef __IO_HANDLERS_H
#define __IO_HANDLERS_H

#include "canfestival.h"
// DS 401 Digital IO handling according DS 401 V2.1 "Device Profile for Generic I/O Modules"


unsigned char digital_input_handler(CO_Data* d, unsigned char *newInput, unsigned char size);

unsigned char digital_output_handler(CO_Data* d, unsigned char *newOuput, unsigned char size);

unsigned char analog_input_handler(CO_Data* d, unsigned int *newInput, unsigned char size);

unsigned char analog_output_handler(CO_Data* d, unsigned int *newOutput, unsigned char size);

#endif
